import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomersPage } from './customers';
import { TranslateModule } from '@ngx-translate/core';
// import { AddDeviceModalPage } from './modals/add-device-modal';

@NgModule({
  declarations: [
    CustomersPage,
    // AddDeviceModalPage
  ],
  imports: [
    IonicPageModule.forChild(CustomersPage),
    TranslateModule.forChild()
  ],
  exports: [
    // AddDeviceModalPage
  ]
})
export class CustomersPageModule {}
