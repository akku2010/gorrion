import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-expense-type-detail',
  templateUrl: 'expense-type-detail.html',
})
export class ExpenseTypeDetailPage {
  type: string;
  expense: any;
  expensesData: any = [];
  portstemp: any[] = [];
  dateFrom: any;
  dateTo: any;
  userId: any;
  vehId: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private translate: TranslateService,
    private alertCtrl: AlertController) {
    if (!navParams.get('expense')) {
      return;
    }
    this.expense = navParams.get('expense');
    this.type = this.expense._id;
    this.dateFrom = navParams.get('dateFrom');
    this.dateTo = navParams.get('dateTo');
    this.userId = navParams.get('userId');
    this.portstemp = navParams.get('portstemp');
    this.vehId = navParams.get('vehId');
  }

  ionViewWillEnter() {
    this.getExpenseList();
  }

  toastMessage(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  getExpenseList() {
    this.expensesData = [];
    let _Url;
    if (this.vehId != undefined) {
      _Url = this.apiCall.mainUrl + "expense/tripdetailbyType?user=" + this.userId + "&fdate=" + new Date(this.dateFrom).toISOString() + "&tdate=" + new Date(this.dateTo).toISOString() + "&type=" + this.expense._id + "&vehicle=" + this.vehId;
    } else {
      _Url = this.apiCall.mainUrl + "expense/tripdetailbyType?user=" + this.userId + "&fdate=" + new Date(this.dateFrom).toISOString() + "&tdate=" + new Date(this.dateTo).toISOString() + "&type=" + this.expense._id;
    }
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_Url)
      .subscribe(data => {
        this.apiCall.stopLoading();
        if (data.message === 'No Expence list found') {
          this.toastMessage(this.translate.instant('Expense list not found..'));
          return
        }
        this.expensesData = data;
        console.log("response data: ", data)
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        })
  }

  delete(exp) {
    let alert = this.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this', { value: this.translate.instant('Expenses') }),
      buttons: [
        {
          text: this.translate.instant('YES PROCEED'),
          handler: () => {
            var _burl = this.apiCall.mainUrl + "expense/deleteExpense?_id=" + exp._id;
            this.apiCall.startLoading().present();
            this.apiCall.getSOSReportAPI(_burl)
              .subscribe(data => {
                this.apiCall.stopLoading();
                if (data) {
                  this.toastMessage(this.translate.instant('Deleted successfully.', { value: this.translate.instant('Expenses') }));
                  this.navCtrl.pop();
                }
                console.log("deleted expense data: ", data)
              },
                err => {
                  this.apiCall.stopLoading();
                  this.toastMessage(this.translate.instant('Deleted successfully.', { value: this.translate.instant('Expenses') }));
                  this.navCtrl.pop();
                })
          }
        },
        {
          text: this.translate.instant('Back')
        }
      ]
    });
    alert.present();
  }

}
